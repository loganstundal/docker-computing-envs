#!/bin/bash

# Named parameters
host_dir=${HOME}
#image=$"rstudio-local"

docker run --tty --interactive --detach --rm --publish 8787:8787 \
  --name rstudio \
  --mount type=bind,source=${host_dir},target=/home/logan/work \
  rstudio:latest

echo -e "\nRstudio is up and reachable at:\n\nhttp://localhost:8787"

