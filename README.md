# Overview

This repository contains Dockerfile build instructions to generate R/Rstudio and Python/Jupyter docker images.

The struggle is real:

![dependency issues](misc/dependency-hell.png "Dependency hell")

To do:

- [ ] placeholder