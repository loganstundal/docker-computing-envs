
# To run container with a default token id
```sh
docker run --rm -ti --name pytest -p 8888:8888  -e JUPYTER_TOKEN=docker python-local
```

The notebook can the always be accessed at [localhost:8888/lab?token=docker].

# resources
- persistent docker token [here](https://towardsdatascience.com/dockerizing-jupyter-projects-39aad547484a)
    - need to embed as env var in dockerfile
- ml environ [here](https://qbiwan.github.io/fastpages/mamba-installation)
