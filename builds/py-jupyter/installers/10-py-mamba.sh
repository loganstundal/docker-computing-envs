#!/bin/bash

# --------------------------------------------- #
# Conda installer - delete me soon
# --------------------------------------------- #
# Download and install miniconda
#wget https://repo.anaconda.com/miniconda/Miniconda3-py310_23.3.1-0-Linux-x86_64.sh
#bash Miniconda3-py310_23.3.1-0-Linux-x86_64.sh -b -f \
#  -p /home/logan/miniconda3

#rm Miniconda3-py310_23.3.1-0-Linux-x86_64.sh

#chown -R logan:users /home/logan/miniconda3

# Initialize conda and restart bash
# https://docs.anaconda.com/free/anaconda/install/silent-mode/
#echo -e '\neval "$(/home/logan/miniconda3/bin/conda shell.bash hook)"' >> /home/logan/.bashrc

# Conda config
#conda config --add channels conda-forge
#conda config --set channel_priority strict
#conda install -y python=3.10 # Force initial packages to conda-forge channel
# --------------------------------------------- #


# --------------------------------------------- #
# Mamba installer (includes conda, but faster)
# --------------------------------------------- #
# https://github.com/conda-forge/miniforge#mambaforge
wget "https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge-$(uname)-$(uname -m).sh"
bash Mambaforge-$(uname)-$(uname -m).sh -b -f \
    -p /home/logan/mambaforge

rm Mambaforge-$(uname)-$(uname -m).sh

# Initialize conda and mamba on user path
# cat << EOF >> /home/logan/.bashrc
#
# source "/home/logan/mambaforge/etc/profile.d/conda.sh"
# conda activate
#
# source "/home/logan/mambaforge/etc/profile.d/mamba.sh"
# mamba activate
# EOF

cat << EOF >> /home/logan/.bashrc

export PATH=$PATH:/home/logan/mambaforge/bin
EOF

# Return mamba dir to user logan
#chown -R logan:users /home/logan/mambaforge

# Give package install script to logan - this is a mess right now
#cp /install_scripts/20-py-packages.sh /home/logan/20-py-packages.sh
#chown logan:users /home/logan/20-py-packages.sh

# Additional documentation:
# https://mamba.readthedocs.io/en/latest/index.html
# --------------------------------------------- #
