# rstudio-prefs.json
This file contains config for the Rstudio-server IDE. It is copied to:
```sh
/home/logan/.config/rstudio/rstudio-prefs.json
```
All possible config options and values are presented [here](https://docs.posit.co/ide/server-pro/reference/session_user_settings.html).

# Themes
* BreezeDark - [source](https://github.com/nnamliehbes/Breeze-RStudio-Theme)
* Material   - [source](https://github.com/lusignan/RStudio-Material-Theme)
* Nord       - [source](https://github.com/lusignan/Nord-RStudio)
