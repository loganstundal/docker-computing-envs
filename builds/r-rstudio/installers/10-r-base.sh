#!/bin/bash

# Install base R

# ----------------------------------- #
# add repos for base-R and CRAN
# ----------------------------------- #
# Add signing key
wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | \
  tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc

# Add the R 4.0 repo from CRAN
add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/"

# Add CRAN repository with pre-compiled binaries
add-apt-repository ppa:c2d4u.team/c2d4u4.0+
# ----------------------------------- #


# ----------------------------------- #
# Install base R
# ----------------------------------- #
apt install -y --no-install-recommends r-base r-base-dev

# Note per r-base-dev:
# Users who need to compile R packages from source [e.g. package maintainers,
# or anyone installing packages with install.packages()] should also install
# the r-base-dev package.
# https://cran.r-project.org/bin/linux/ubuntu/fullREADME.html
# ----------------------------------- #


# ----------------------------------- #
# User directories for R
# ----------------------------------- #
# user logan exists. created in ubuntu-base image setup
mkdir -p /home/logan/R/R-library

# Set up working directory mount point
mkdir -p /home/logan/work
# ----------------------------------- #


# ----------------------------------- #
# Import custom R config
# ----------------------------------- #
# custom R profile
mv /setup/logan.Rprofile /home/logan/R/logan.Rprofile

# Update Renviron to reflect profile and custom user lib
cat << EOF >> /etc/R/Renviron.site
R_LIBS_USER=/home/logan/R/R-library
R_PROFILE_USER=/home/logan/R/logan.Rprofile
EOF
# ----------------------------------- #
