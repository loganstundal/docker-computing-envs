#!/bin/bash

# Install R-stan

# ----------------------------------- #
# Installation note
# ----------------------------------- #
# A simple `apt install r-cran-rstan` installation does install
# rstan and all dependencies. However, the stan_model() function does not
# properly compile models despite producing no errors. Even when sampling the
# sampling() example [a simple sample from a normal] it produes diverdent steps
# equal to the itrations. This approach instead installs rstan while avoiding
# this problem.
# ----------------------------------- #


# ----------------------------------- #
# Pre-compiled binaries
# ----------------------------------- #
# Install dependencies with apt to minimize compiles
apt install -y        \
  r-cran-bh           \
  r-cran-checkmate    \
  r-cran-gridextra    \
  r-cran-inline       \
  r-cran-loo          \
  r-cran-matrixstats  \
  r-cran-rcppeigen    \
  r-cran-rcppparallel \
  r-cran-pkgbuild     \
  r-cran-v8
# ----------------------------------- #


# ----------------------------------- #
# rstan and stanheaders
# ----------------------------------- #
#R -e 'install.packages("rstan", repos = c("https://mc-stan.org/r-packages/", getOption("repos")))'
R -e 'install.packages("rstan", lib = "/home/logan/R/R-library")'
# ----------------------------------- #


# ----------------------------------- #
# Repair home directory ownership
# ----------------------------------- #
chown -R logan:users /home/logan/R
# ----------------------------------- #
