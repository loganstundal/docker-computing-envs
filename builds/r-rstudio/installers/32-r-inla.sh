#!/bin/bash

# Install R-INLA
# https://www.r-inla.org/


# ----------------------------------- #
# Pre-compiled binaries
# ----------------------------------- #
# Install dependencies with apt to minimize compiles
apt install -y         \
    r-cran-mcmc        \
    r-cran-mcmcpack    \
    r-cran-gtools      \
    r-cran-htmlwidgets \
    r-cran-httpuv      \
    r-cran-xtable      \
    r-cran-sourcetools \
    r-cran-later       \
    r-cran-promises    \
    r-cran-deriv       \
    r-cran-gsl         \
    r-cran-numderiv    \
    r-cran-pixmap      \
    r-cran-rgl         \
    r-cran-shiny       \
    r-cran-sn          \
    r-cran-splancs     \
    r-cran-terra
# ----------------------------------- #


# ----------------------------------- #
# R-INLA and non-cran dependencies
# ----------------------------------- #
R -e 'install.packages("INLA",repos=c(getOption("repos"),INLA="https://inla.r-inla-download.org/R/stable"), dep=TRUE, lib = "/home/logan/R/R-library")'
# ----------------------------------- #

# ----------------------------------- #
# Repair home directory ownership
# ----------------------------------- #
# Note:
# This is especially necessary for r-inla. INLA writes to its install directory
# rather than other accessible temp locations. Without write permissions to the
# directory, the basic inla() command to fit a model WILL fail.
chown -R logan:users /home/logan/R
# ----------------------------------- #
