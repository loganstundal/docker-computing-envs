#!/bin/bash

# Install r-packages

apt install -y \
  r-cran-devtools     \
  r-cran-doparallel   \
  r-cran-ecdat        \
  r-cran-evd          \
  r-cran-fields       \
  r-cran-janitor      \
  r-cran-lmtest       \
  r-cran-markdown     \
  r-cran-mlogit       \
  r-cran-mnormt       \
  r-cran-quantreg     \
  r-cran-sf           \
  r-cran-spatialreg   \
  r-cran-spdep        \
  r-cran-texreg       \
  r-cran-tidyverse    \
  r-cran-usethis      \
  r-cran-viridis
