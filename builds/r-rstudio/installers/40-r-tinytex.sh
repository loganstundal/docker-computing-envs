#!/bin/bash

# ----------------------------------- #
# Install tinytex per Yihui's instructions
# ----------------------------------- #
# https://yihui.org/tinytex/faq/
wget -qO- "https://yihui.org/tinytex/install-unx.sh" \
  | sh -s - --admin --no-path
# This will install Tinytex to ~/.TinyTex
# ----------------------------------- #


# ----------------------------------- #
# Modify install point and path
# ----------------------------------- #
# Move to user (logan) directory
mv /root/.TinyTeX /home/logan

# Update path
/home/logan/.TinyTeX/bin/*/tlmgr path add
# ----------------------------------- #

# ----------------------------------- #
# Install relevant r-packages for pdf/html documents
# ----------------------------------- #
apt install -y     \
  r-cran-tinytex   \
  r-cran-bookdown  \
  r-cran-base64enc \
  r-cran-digest    \
  r-cran-evaluate  \
  r-cran-glue      \
  r-cran-highr     \
  r-cran-htmltools \
  r-cran-jsonlite  \
  r-cran-magrittr  \
  r-cran-markdown  \
  r-cran-mime      \
  r-cran-rmarkdown \
  r-cran-stringi   \
  r-cran-stringr   \
  r-cran-yaml
# ----------------------------------- #
