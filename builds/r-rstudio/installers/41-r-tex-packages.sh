#!/bin/bash

# I have no idea what 99.9% of these packages do, but true to latex, if
# you need to so much as move a period you'll need at least 20 new packages.

tlmgr install \
  bookmark    \
  caption     \
  environ     \
  koma-script \
  multirow    \
  pdfcol      \
  pgf         \
  tcolorbox   \
  tikzfill    \
