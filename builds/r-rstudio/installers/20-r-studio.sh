#!/bin/bash

# Install R-Studio Server

# ----------------------------------- #
# Download and install
# ----------------------------------- #
# Download
wget https://download2.rstudio.org/server/jammy/amd64/rstudio-server-2023.06.0-421-amd64.deb

# Install
gdebi --non-interactive rstudio-server-2023.06.0-421-amd64.deb
# ----------------------------------- #


# ----------------------------------- #
# Configure Rstudio
# ----------------------------------- #
# Create R-Studio configuration directories
mkdir -p /home/logan/.config/rstudio/{snippets,templates,themes}

# Import Rstudio preferences
mv /setup/rstudio-prefs.json /home/logan/.config/rstudio/rstudio-prefs.json

# Import user code snippets
mv /setup/r.snippets /home/logan/.config/rstudio/snippets/

# Import default file templates
mv /setup/default.* /home/logan/.config/rstudio/templates/

# Import custom themes (see here for more: https://github.com/max-alletsee/rstudio-themes)
mv /setup/*.rstheme /home/logan/.config/rstudio/themes/
# ----------------------------------- #


# ----------------------------------- #
# Clean-up
# ----------------------------------- #
rm rstudio-server-2023.06.0-421-amd64.deb
# ----------------------------------- #
