This repository contains DOCKERFILE scripts to build containerized environments for running R/Rstudio.

# Directory structure
```
.
├── config
│   ├── BreezeDark.rstheme
│   ├── default.Qmd
│   ├── default.R
│   ├── logan.Rprofile
│   ├── Material.rstheme
│   ├── Nord.rstheme
│   ├── README.md
│   ├── r.snippets
│   └── rstudio-prefs.json
├── Dockerfile
├── installers
│   ├── 10-r-base.sh
│   ├── 20-r-studio.sh
│   ├── 30-r-packages.sh
│   ├── 31-r-stan.sh
│   ├── 32-r-inla.sh
│   ├── 40-r-tinytex.sh
│   └── 41-r-tex-packages.sh
└── README.md
```

# Working
- [x] Rstudio server
- [x] Tidyverse
- [x] R-Stan
- [x] R-INLA
- [x] image mount point at `/home/logan/work`
- [x] Rstudio config incorporated (settings and layout preferences)
- [x] Add default R scripts to .config/rstudio. Examples: [here](https://github.com/rstudio/rstudio/pull/5144) and [here](https://docs.posit.co/ide/server-pro/1.3.399-1/r-sessions.html#default-document-templates) for more details.
- [x] tinytex install and setup

# To do
- [ ] integrate pdf live preview (Posit note [here](https://support.posit.co/hc/en-us/articles/200552066-PDF-Preview-and-SyncTeX) and SyncTex [here](https://itexmac.sourceforge.net/SyncTeX.html))
- [ ] update all readmes with better instructions.

# Long-term
- [ ] Deploy this image on my server and volume-map the rstudio container to the nexcloud container with my working project directory (maybe with symbolic links due to www-data ownership on the nc side). Then just remote connect via vpn tunnel as usual and never bother deploying the image to any localhost.

-----------------------------------

THESE instructions are dated

# Usage
## To build
```sh
git clone https://gitlab.con/loganstundal/docker-computing-envs.git
cd docker-computing-envs
docker build -t <optional-name-to-call-image> .  
```

## Starting a container
The default mount point within the container is `/home/logan/work`. Bind mounts like this are best for the intended use case of this image. Volumes are not ideal here since the point is to selectively point the container at project directory only when in use.

```sh
docker run --rm -ti -d --name <name-for-container> -p 8787:8787 \
  --mount type=bind,source=<path/on/host/machine>,target=/home/logan/work \
  rstudio-local
```

## Accessing the rstudio server
The server deploys on the host machine of the docker instance and can be reached at `localhost:8787` from a browser. The server username and password are both `logan`. This value can be modified in `00-ubuntu-setup.sh`. However, it will likely break directory paths assigned in subsequent scripts resulting in a build failure.


## Stopping a container
```sh
docker container stop name-for-container
```

## To preserve changes made in a container
The dockerfile should be updated as that is best practice. However, if preferring to save newly installed packages such as installed with `install.packages()` from within R without editing the Dockerfile, then:

1. install relevant packages within the container.
2. with the container still running (e.g., in the `docker container ls` list output), execute:

```sh
docker commit <container-name> <OPTIONAL new-image-tag>
```

## Saving a portable image tarball
```sh
#####docker image [OPTIONAL -o <filename>] save <image-name>
## Example, to save image bob
#####docker image save -o /home/logan/bob-save bob

docker save --output my-file.tar myimage
# Exampel to save an image called projectA to a file called backup-projectA.tar:
docker save --output backup-projectA.tar projectA
```

## Importaing a saved tarball file
```sh
#docker image import <tarball-file-name>

docker load --input mifile.tar
```
