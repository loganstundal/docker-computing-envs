#!/bin/bash

export DEBIAN_FRONTEND="noninteractive"

# ----------------------------------- #
# Create directory for subsequent installs
# ----------------------------------- #
mkdir /setup
# ----------------------------------- #


# ----------------------------------- #
# System update and package installs
# ----------------------------------- #
apt update -y && apt upgrade -y

apt install -y               \
  apt-utils                  \
  curl                       \
  dirmngr                    \
  g++                        \
  gdebi-core                 \
  gpg                        \
  libcurl4-openssl-dev       \
  locales                    \
  make                       \
  nano                       \
  software-properties-common \
  sudo                       \
  wget
# ----------------------------------- #


# ----------------------------------- #
# Locales and user
# ----------------------------------- #
# Set up locales
locale-gen en_US.UTF-8

# set up user account
# User = logan
# Pass = logan
useradd -m -g users logan
usermod -aG sudo logan # to install pacakges on the fly with apt
echo logan:logan | chpasswd # pipe password=logan for user=logan into chpasswd command
#usermod -aG sudo logan
# ----------------------------------- #
